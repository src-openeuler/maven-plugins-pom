%global short_name maven-plugins

Name:           maven-plugins-pom
Version:        28
Release:        9
Summary:        Maven Plugins POM
License:        ASL 2.0
URL:            http://maven.apache.org/plugins/
BuildArch:      noarch
Source:         https://repo.maven.apache.org/maven2/org/apache/maven/plugins/%{short_name}/%{version}/%{short_name}-%{version}-source-release.zip

BuildRequires:  maven-local mvn(org.apache.maven:maven-parent:pom:) mvn(org.apache.maven.plugins:maven-plugin-plugin) mvn(org.apache.maven.plugin-tools:maven-plugin-annotations)

%description
This package provides Maven Plugins parent POM used by different
Apache Maven plugins.

%prep
%autosetup -n maven-plugins-%{version} -p1
%pom_remove_plugin :maven-enforcer-plugin
%pom_remove_plugin :maven-scm-publish-plugin
%pom_remove_plugin :maven-site-plugin

%build
%mvn_build

%install
%mvn_install

%files -f .mfiles
%doc LICENSE NOTICE

%changelog
* Tue Mar 3 2020 wangye <wangye54@huawei.com> - 28-9
- Package init
